using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighlightHexTile : MonoBehaviour
{
    [SerializeField] private bool isFull = false;
    public GameObject[] HighlightedHexTiles;

    private GameObject _gameObject;
    private Material _colorToChange;

    private void Awake()
    {
        isFull = false;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0)) 
        {
            if (isFull) // If the Hex Tile array is full, it will clear all elements.
            {
                ClearTileArray();
                isFull = false;
            }

            GetTile();
        }
    }

    private void GetTile () 
    {
        RaycastHit _hit;
        Ray _ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(_ray, out _hit, 100f))
        {
            if (_hit.transform) // If the Raycast hits a GameObject it will store it in _gameObject
            {
                _gameObject = _hit.transform.gameObject;

                if (_gameObject.tag == "Wakable")
                {
                    ChangeColorTile();
                    SaveTileToArray();

                } else { Debug.Log("You cant walk on water"); }
            }
        }
    }

    private void SaveTileToArray() // Saves the GameObject that we from GetTile to the array _highlightedHexTiles[]
    {
        int i;
        for (i = 0; i < HighlightedHexTiles.Length; i++)
        {

            if (HighlightedHexTiles[i] == null)
            {
                HighlightedHexTiles[i] = _gameObject;
                break;
            }
        }

        if (i == HighlightedHexTiles.Length - 1) 
        {
            isFull = true;
        }
    }

    private void ClearTileArray() // We will use this when the array is full, reseting the higlighted color first with ChangeColorTile()
    {
        int i;
        for (i = 0; i < HighlightedHexTiles.Length; i++)
        {
            _gameObject = HighlightedHexTiles[i];
            ChangeColorTile();
            HighlightedHexTiles[i] = null;
        }
    }
        
    private void ChangeColorTile() // Gets the current Material of the Tile, channging it to Green if its beeing added to the array and if its beeing Cleared by the ClearTileArray it will reset color to White.
    {
        _colorToChange = _gameObject.GetComponent<Renderer>().material;
        
        if (isFull)
        {
            _colorToChange.color = Color.white;
        }
        else
        {
            _colorToChange.color = Color.green;
        }
    
    }
}

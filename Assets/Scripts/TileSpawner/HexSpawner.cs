using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexSpawner : MonoBehaviour
{
    public GameObject[] HexTilesPrefabs;

    [SerializeField] private int _rows;     // Hexagon map width
    [SerializeField] private int _columns;  // Hexagon map height

    [SerializeField] private float _hexTileOffSetX = 0f;
    [SerializeField] private float _hexTileOffSetZ = 0f;

    private Vector3 _gridOrigin = Vector3.zero;

    private void Start()
    {
        SpawnHexGrid();
    }

    private void SpawnHexGrid()
    {
        for (int x = 0; x < _rows; x++)
        {
            for (int z = 0; z < _columns; z++)
            {
                int _random = Random.Range(0, HexTilesPrefabs.Length); // Pick a random number from 0 to the length of hexTilesPrefab
                GameObject _hex = Instantiate(HexTilesPrefabs[_random], _gridOrigin, this.transform.rotation); // Spawn a random prefab on the gridOrigin with rotation 0.
                
                _hex.transform.parent = transform; // Set the Empty Game Object with this Script to be the parent of every tile created

                if (z % 2 == 0) // Every second hex tile will be offset for a certain value
                {
                    _hex.transform.position = new Vector3(x * _hexTileOffSetX, 0, z * _hexTileOffSetZ);
                }
                else // Else we will move the X position for a certain value
                {
                    _hex.transform.position = new Vector3(x * _hexTileOffSetX + _hexTileOffSetX / 2, 0, (z * _hexTileOffSetZ));
                }
            }

        }

    }
}

